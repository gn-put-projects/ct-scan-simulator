import math
import numpy as np
from skimage.draw import line_nd as generate_line

class Ctsim():
    def __init__(self, image_array, range_degrees, emiter_count, iteration_count, rotation):
        self.image_array = image_array
        self.height, self.width = image_array.shape
        self.radius = math.sqrt(self.width**2 + self.height**2) // 2
        self.range_rad = math.radians(range_degrees)
        self.emiter_count = emiter_count
        self.iteration_count = iteration_count
        self.rotation_rad = math.radians(rotation)
        
        self.sinogram = np.zeros((self.emiter_count, self.iteration_count))
        self.filterless_sinogram = np.zeros((self.emiter_count, self.iteration_count))
        self.color_array = np.zeros((self.height, self.width))
        self.filterless_color_array = np.zeros((self.height, self.width))

    def get_lines(self, i):
        output = list()
        rotation = i * self.rotation_rad
        (x0, y0) = self.generate_emiter_position(rotation)
        for emiter in range(self.emiter_count):
            (x1, y1) = self.generate_detector_position(rotation, emiter)
            output.append(list(generate_line((x0, y0), (x1, y1), endpoint=True)))
        return output
    
    def generate_emiter_position(self, current_rotation):
        # Calculate current emiter point 
        x0 = math.floor(self.radius*math.cos(current_rotation) + self.width/2)
        y0 = math.floor(self.radius*math.sin(current_rotation) + self.height/2)

        return (x0, y0)

    def generate_detector_position(self, current_rotation, detector):
        x1 = math.floor(self.radius*math.cos(current_rotation + math.pi - self.range_rad/2 + detector*self.range_rad/(self.emiter_count-1)) + self.width/2)
        y1 = math.floor(self.radius*math.sin(current_rotation + math.pi - self.range_rad/2 + detector*self.range_rad/(self.emiter_count-1)) + self.height/2)

        return (x1, y1)
        
    def transformation(self):
        for i in range(self.iteration_count):
            current_rotation = i * self.rotation_rad

            # Calculate current emiter point 
            (x0, y0) = self.generate_emiter_position(current_rotation)

            # Calculate ending points of the line
            for detector in range(self.emiter_count):
                (x1, y1) = self.generate_detector_position(current_rotation, detector)

                line_coordinates = list(generate_line((x0, y0), (x1, y1), endpoint=True))

                pixel_sum = 0
                num_of_pixels = 0

                for j in range(len(line_coordinates[0])):
                    x = line_coordinates[0][j]
                    y = line_coordinates[1][j]
                    if x < 0 or x >= self.width or y < 0 or y >= self.height:
                        continue
                    #if(self.image_array[y][x] != 0):
                    num_of_pixels += 1
                    pixel_sum += self.image_array[y][x]

                if(num_of_pixels != 0):    
                    self.sinogram[detector][i] = pixel_sum/num_of_pixels  
                    self.filterless_sinogram[detector][i] = pixel_sum/num_of_pixels  
        
         
        if min(self.sinogram.shape) < 21:
            print("Sinogram too small for filtering")
            return

        kernel = [None] * 21

        kernel[10] = 1
        
        for i in range(1, 11):
            if i % 2 == 0:
                kernel[10-i] = 0
                kernel[10+i] = 0
            else:
                kernel[10-i] = (-4/(math.pi**2))/i**2
                kernel[10+i] = (-4/(math.pi**2))/i**2

        for i in range(self.sinogram.shape[0]):
            self.sinogram[i,:] = np.convolve(self.sinogram[i,:], kernel, mode='same')
        
    def reverseTransformation(self):
        help_array = np.zeros((self.height, self.width))
                
        for i in range(self.iteration_count):
            current_rotation = i * self.rotation_rad

            # Calculate current emiter point 
            (x0, y0) = self.generate_emiter_position(current_rotation)
            
            # Calculate ending points of the line
            for detector in range(self.emiter_count):
                (x1, y1) = self.generate_detector_position(current_rotation, detector)
                    
                line_coordinates = list(generate_line((x0, y0), (x1, y1), endpoint=True))
                
                for j in range(len(line_coordinates[0])):
                    x = line_coordinates[0][j]
                    y = line_coordinates[1][j]
                    if x < 0 or x >= self.width or y < 0 or y >= self.height:
                        continue

                    self.color_array[y][x] += self.sinogram[detector][i]
                    self.filterless_color_array[y][x] += self.filterless_sinogram[detector][i]
                    help_array[y][x] += 1

        (height, width) = self.color_array.shape

        for y in range(height):
            for x in range(width):
                if help_array[y][x] == 0.0:
                    continue
                self.color_array[y][x] /= help_array[y][x]
                self.filterless_color_array[y][x] /= help_array[y][x]

        minimal_value = np.min(self.color_array)
        self.color_array -= minimal_value
        maximal_value = np.max(self.color_array)
        self.color_array *= 255/(maximal_value-minimal_value)
        
        minimal_value = np.min(self.filterless_color_array)
        self.filterless_color_array -= minimal_value
        maximal_value = np.max(self.filterless_color_array)
        self.filterless_color_array *= 255/(maximal_value-minimal_value)

        minimal_value = np.min(self.filterless_sinogram)
        self.filterless_sinogram -= minimal_value
        maximal_value = np.max(self.filterless_sinogram)
        self.filterless_sinogram *= 255/(maximal_value-minimal_value)

    def getArr(self):
        self.transformation()
        self.reverseTransformation()

        return (self.filterless_sinogram, self.color_array, self.filterless_color_array)
