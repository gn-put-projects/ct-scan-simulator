from pydicom.dataset import Dataset, FileDataset
from pydicom.uid import ExplicitVRLittleEndian
from pydicom.filereader import dcmread
import pydicom._storage_sopclass_uids

import numpy as np

from skimage.util import img_as_ubyte
from skimage.exposure import rescale_intensity
from ctsim.dicom.patient import Patient

class Dicom():
    def __init__(self, patient_name = "Robert Mazurek", patient_id = "123456789", image_comments = "Komentarz do obrazu"):
        self.patient = Patient(patient_name, patient_id, image_comments, None)

    def convert_image_to_ubyte(self, img):
        imgarray = np.array(img)
        return img_as_ubyte(rescale_intensity(imgarray, out_range=(0.0, 1.0)))


    def save_as_dicom(self, file_name, img):
        img = self.convert_image_to_ubyte(img)
        # Populate required values for file meta information
        meta = Dataset()
        meta.MediaStorageSOPClassUID = pydicom._storage_sopclass_uids.CTImageStorage
        meta.MediaStorageSOPInstanceUID = pydicom.uid.generate_uid()
        meta.TransferSyntaxUID = pydicom.uid.ExplicitVRLittleEndian  

        ds = FileDataset(None, {}, preamble=b"\0" * 128)
        ds.file_meta = meta

        ds.is_little_endian = True
        ds.is_implicit_VR = False

        ds.SOPClassUID = pydicom._storage_sopclass_uids.CTImageStorage
        ds.SOPInstanceUID = meta.MediaStorageSOPInstanceUID

        ds.PatientName = self.patient.name
        
        ds.PatientID = self.patient.id
        ds.ImageComments = self.patient.image_comments

        ds.Modality = "CT"
        ds.SeriesInstanceUID = pydicom.uid.generate_uid()
        ds.StudyInstanceUID = pydicom.uid.generate_uid()
        ds.FrameOfReferenceUID = pydicom.uid.generate_uid()

        ds.BitsStored = 8
        ds.BitsAllocated = 8
        ds.SamplesPerPixel = 1
        ds.HighBit = 7

        ds.ImagesInAcquisition = 1
        ds.InstanceNumber = 1

        ds.Rows, ds.Columns = img.shape

        ds.ImageType = r"ORIGINAL\PRIMARY\AXIAL"

        ds.PhotometricInterpretation = "MONOCHROME2"
        ds.PixelRepresentation = 0

        pydicom.dataset.validate_file_meta(ds.file_meta, enforce_standard=True)

        ds.PixelData = img.tobytes()
        ds.save_as(file_name, write_like_original=False)
        
    def read_from_dicom(self, path):
        ds = dcmread(path, force=True)

        if not hasattr(ds, 'PatientID'):
            raise Exception('Patient ID not defined in the read dicom file')

        if not hasattr(ds, 'PatientName'):
            raise Exception('Patient ID not defined in the read dicom file')

        if not hasattr(ds, 'ImageComments'):
            raise Exception('Patient ID not defined in the read dicom file')
        
        pid = ds.PatientID
        name = ds.PatientName
        image_comments = ds.ImageComments
        self.patient = Patient(name, pid, image_comments, ds.pixel_array)

        return self.patient
