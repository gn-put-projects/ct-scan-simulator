#### Computed Tomography Scan Simulator
A GUI program for CT scan simulation on given bitmap images. Simulates 1D scans with the [Radon Transformation](https://en.wikipedia.org/wiki/Radon_transform) and reconstructs the image in 2D using the Inverse Radon Transformation.
