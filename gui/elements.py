import tkinter as tk
from PIL import Image, ImageTk, ImageDraw

class GuiImage():
    def __init__(self, win, label_text, row, col, fromfile=""):
        self.text = win.make_label(label_text)
        self.text.grid(row=row, column=col, columnspan=1, padx=10, pady=10)
        self.label = tk.Label(win, image=None)
        self.label.grid(row=row+1, column=col, columnspan=1, padx=10, pady=10)

        self.display = None
        self.real = None

        if (fromfile != ""):
            self.set_from_file(fromfile)

    def set_from_photo(self, photo):
        None

    def display_reset(self):
        self.display = self.real.copy()

    def display_resize(self, max_dim):
        scale = max_dim/max(self.display.size)
        new_display_size = tuple([
            int(scale*self.display.size[0]),
            int(scale*self.display.size[1])
        ])
        
        self.display = self.display.resize(new_display_size, resample=Image.BOX)
        
        self.commit_display()
        
    def commit_display(self):
        self.photo = ImageTk.PhotoImage(self.display)
        self.label.config(image=self.photo)
        
    def set_from_file(self, path):
        self.real = Image.open(path).convert('L')
        self.display_reset()
        
        self.commit_display()

    def set_empty(self, size_tuple):
        self.real = Image.new("L", size_tuple)
        self.display_reset()
        self.commit_display()

    def set_real(self, img):
        self.real = img
        self.display_reset()
        self.commit_display()

        
class Entry():
    def __init__(self, win, default_value, integer=True, string=False):
        self.win = win
        self.is_int = integer
        self.is_string = string
        self.default_value = default_value
        self.val_cmd = None if string else self.win.int_val_cmd if integer else self.win.num_val_cmd
        self.entry = tk.Entry(self.win, validate="key", validatecommand=self.val_cmd)
        self.entry.bind("<FocusIn>", self.clear_default)
        self.entry.bind("<FocusOut>", self.restore_default)
        self.restore_default()

    def get(self):
        if (self.is_string):
            return str(self.entry.get())
        if (self.is_int):
            return int(self.entry.get())
        return float(self.entry.get())

    def set(self, newval):
        self.entry.delete(0, tk.END)
        self.entry.insert(0, newval)
    
    def clear_default(self, event=None):
        if self.get() == self.default_value:
            self.entry.delete(0, tk.END)
            self.entry.config(fg='black')

    def restore_default(self, event=None):
        if self.entry.get() == '':
            self.entry.insert(0, self.default_value)
            self.entry.config(fg='grey')
            
    def position(self, row, col):
        self.entry.grid(row=row, column=col, columnspan=1, pady=10, padx=10)
