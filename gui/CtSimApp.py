import tkinter as tk
import numpy as np
from PIL import Image, ImageTk, ImageDraw
from gui.elements import Entry, GuiImage
import math
from ctsim.ctsim import Ctsim
from tkinter import filedialog as fd
from ctsim.dicom.dicom import Dicom
from ctsim.dicom.patient import Patient

def int_val_fn(action, index, value_if_allowed, prior_value, text, validation_type, trigger_type, widget_name):
    if not value_if_allowed or action != '1':
        return True

    if value_if_allowed.isdigit() and len(value_if_allowed) < 12:
        return True
    return False
    
def num_val_fn(action, index, value_if_allowed, prior_value, text, validation_type, trigger_type, widget_name):
    if not value_if_allowed or action != '1':
        return True
            
    if value_if_allowed.replace('.', '', 1).isdigit() and len(value_if_allowed) < 5:
        return True
    return False

class CtSimApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("CT Scan Simulator")
        self.image_size = 250
        self.current_index = 0
        self.resizing = True
        self.ctsim = None
                
        self.sinogram = None
        self.ctsim = None
        self.dicom = Dicom()
        self.patient = Patient(None, None, None, None)

        self.create_layout()

    def set_dicom_data():
        print("s")

    def make_label(self, text, font_size=24):
        return tk.Label(self, text=text, compound='center', font=("Arial", font_size))

    def open_file(self):
        filepath = fd.askopenfilename(
            title='Open a file',
            initialdir='./res/'
        )
        if (filepath == ""): return
        
        self.original_image.set_from_file(filepath)
                
        self.update_vis()

    def open_dicom_file(self):
        filepath = fd.askopenfilename(
            title='Open a dicom file',
            initialdir='./res/dicom/',
            filetypes=[('DICOM file', '*.dcm')]
        )
        if (filepath == ""): return
        
        patient = self.dicom.read_from_dicom(filepath)
        self.update_patient_data_display(patient)
        
    def update_patient_data_display(self, patient):
        img = Image.fromarray(patient.pixel_array)
        self.original_image.set_real(img)

        self.pname_entry.set(patient.name)
        self.pid_entry.set(patient.id)
        self.imagecomment_entry.set(patient.image_comments)
        self.update_vis()

    def sync_patient_data_with_inputs(self):
        self.dicom.patient.pixel_array = np.asarray(self.original_image.real)
        self.dicom.patient.name = self.pname_entry.get()
        self.dicom.patient.id = self.pid_entry.get()
        self.dicom.patient.image_comments = self.imagecomment_entry.get()
        
    def save_dicom_file(self):
        filepath = fd.asksaveasfilename(
            title='Save',
            initialdir='./res/dicom/',
            defaultextension=".dcm"
        )
        if (filepath == ""): return

        self.sync_patient_data_with_inputs()
        self.dicom.save_as_dicom(filepath, self.output_image.real)
                                 
    def create_layout(self):
        top_row = 0
        self.num_val_cmd = (self.register(num_val_fn), '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.int_val_cmd = (self.register(int_val_fn), '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        # Dicom input / output
        self.open_dicom = tk.Button(self, text="Open a dicom file", command=self.open_dicom_file)
        self.open_dicom.grid(row=self.image_size, column=0, columnspan=1, pady=10, padx=10)

        self.open_dicom = tk.Button(self, text="Save as a dicom file", command=self.save_dicom_file)
        self.open_dicom.grid(row=self.image_size, column=1, columnspan=1, pady=10, padx=10)

        self.pname_label = tk.Label(self, text='Patient Name:', compound='center', font=("Arial", 15))
        self.pname_label.grid(row= 0 , column=5, columnspan=1, padx=10, pady=10)
        self.pid_label = tk.Label(self, text='Patient id:', compound='center', font=("Arial", 15))
        self.pid_label.grid(row= 2 , column=5, columnspan=1, padx=10, pady=10)
        self.imagecomment_label = tk.Label(self, text='Image comment:', compound='center', font=("Arial", 15))
        self.imagecomment_label.grid(row= 4 , column=5, columnspan=1, padx=10, pady=10)

        self.pname_entry = Entry(self, self.dicom.patient.name, string=True)
        self.pname_entry.position(1, 5)

        self.pid_entry = Entry(self, self.dicom.patient.id, string=True)
        self.pid_entry.position(3, 5)

        self.imagecomment_entry = Entry(self, self.dicom.patient.image_comments, string=True)
        self.imagecomment_entry.position(5, 5)

        # End dicom input / output
        self.file_dialog = tk.Button(self, text="Open a file", command=self.open_file)
        self.file_dialog.grid(row=top_row, column=0, columnspan=1, pady=10, padx=10)
        self.grid_rowconfigure(top_row, weight=0)
        self.spacing_label = tk.Label(self, text='Detector spacing (deg)', compound='center', font=("Arial", 15))
        self.spacing_label.grid(row=top_row, column=1, columnspan=1, padx=10, pady=10)
        self.ec_label = tk.Label(self, text='Emiter count', compound='center', font=("Arial", 15))
        self.ec_label.grid(row=top_row, column=2, columnspan=1, padx=10, pady=10)
        self.ic_label = tk.Label(self, text='Iteration count', compound='center', font=("Arial", 15))
        self.ic_label.grid(row=top_row, column=3, columnspan=1, padx=10, pady=10)
        self.rotation_label = tk.Label(self, text='Rotation angle (deg)', compound='center', font=("Arial", 15))
        self.rotation_label.grid(row=top_row, column=4, columnspan=1, padx=10, pady=10)
        
        input_row = 1
        self.grid_rowconfigure(input_row, weight=0)
        self.button = tk.Button(self, text="Start", command=self.start_onclick)
        self.button.grid(row=input_row, column=0, columnspan=1, pady=10, padx=10)
        
        self.spacing_entry = Entry(self, 50, integer=False)
        self.spacing_entry.position(input_row, 1)

        self.ec_entry = Entry(self, 50)
        self.ec_entry.position(input_row, 2)
        
        self.ic_entry = Entry(self,89)
        self.ic_entry.position(input_row, 3)
            
        self.rotation_entry = Entry(self, 4, integer=False)
        self.rotation_entry.position(input_row, 4)
        
        self.grid_rowconfigure(2, weight=0)
        self.grid_rowconfigure(3, weight=0)

        vis_row = 2
        self.original_image = GuiImage(self, 'Original Image', row=vis_row, col=0, fromfile="./res/Kropka.jpg")
        self.sinogram_image = GuiImage(self, 'Sinogram', row=vis_row, col=1)
        self.output_image = GuiImage(self, 'Output', row=vis_row, col=2)
        self.filterless_image = GuiImage(self, 'Filterless output', row=vis_row, col=3)
                
        self.slider = tk.Scale(self, from_=0, to=5, orient=tk.HORIZONTAL, command=self.slider_onchange)
        self.slider.grid(row=vis_row+2, column=1, columnspan=3, sticky='EW', pady=10, padx=10)
        self.slider.grid_remove()

        self.update_vis()

        
        settings_row=4

    def start_onclick(self):
        self.spacing_entry.restore_default()
        self.ic_entry.restore_default()
        self.ec_entry.restore_default()
        self.rotation_entry.restore_default()

        self.read_data_from_inputs()
        self.slider.config(from_=1, to=self.iteration_count)
        self.slider.grid()

        self.process_image()

    def read_data_from_inputs(self):
        self.range_degrees = self.spacing_entry.get()
        self.emiter_count=self.ec_entry.get()
        self.iteration_count=self.ic_entry.get()
        self.rotation=self.rotation_entry.get()
        self.ctsim = Ctsim(image_array=np.array(self.original_image.real),
                                      range_degrees=self.range_degrees,
                                      emiter_count=self.emiter_count,
                                      iteration_count=self.iteration_count,
                                      rotation=self.rotation)
        
    def slider_onchange(self, iteration):
        iteration = int(iteration) - 1
        if (self.ctsim == None):
            return
        lines = self.ctsim.get_lines(iteration)

        self.original_image.display_reset()
        
        width, height = self.original_image.display.size
        
        for l in range(len(lines)):
            line = lines[l] 
            for j in range(len(line[0])):
                x = line[0][j]
                y = line[1][j]
                if x < 0 or x >= width or y < 0 or y >= height:
                    continue
                self.original_image.display.putpixel((x,y), 255)

        
        self.sinogram_image.display_reset()
        drawer = ImageDraw.Draw(self.sinogram_image.display)
        shape = [(iteration+1, 0), self.sinogram_image.display.size] 
        drawer.rectangle(shape, fill="#ffffff")

        self.update_vis()
        
    def update_vis(self):
        if self.resizing:
            self.original_image.display_resize(self.image_size)
        
        if self.ctsim == None:
            return

        self.sinogram_image.display_resize(self.image_size)
        self.output_image.display_resize(self.image_size)
        self.filterless_image.display_resize(self.image_size)
        
    def process_image(self):
        (sinogram_arr, output_arr, filterless_output_arr) = self.ctsim.getArr()
        
        self.sinogram_image.set_real(Image.fromarray(sinogram_arr))
        self.output_image.set_real(Image.fromarray(output_arr))
        self.filterless_image.set_real(Image.fromarray(filterless_output_arr))
        self.update_vis()
